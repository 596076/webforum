import {useEffect, useState} from 'react';
import {returnAllCategoryTopics, updateTopicCounter} from '../../services/topics.service.js';
import './Debug.css';
import {addDoc, collection, getDocs, orderBy, query, serverTimestamp} from 'firebase/firestore';
import {auth, db} from '../../config/firebase-config.js';
import {useNavigate} from 'react-router-dom';
import {createPostToTopicRecord} from '../../services/posts.service.js';

let tempData = {};
export const Debug = () => {
  const navigate = useNavigate();
  const [allTopics, setAllTopics] = useState([]);
  const [multipleTags, setMultipleTags] = useState([]);
  const [shouldRedirect, setShouldRedirect] = useState(false);

  const [category, setCategory] = useState('Android');
  const [topic, setTopic] = useState('');
  const [post, setPost] = useState('');

  const returnTopicReplies = (topicuid, category,title) => {
    const allPosts = [];
    const q = query(collection(db, `/Categories/${category}/topics/${topicuid}/posts`), orderBy("createDate"));
    getDocs(q).then(documents => {
      documents.forEach((d) =>
          allPosts.push(d.data())
        // console.log(`related posts: ${JSON.stringify(d.data())}`))
      );
      navigate(`/start/debug/${category.toLowerCase()}/${topicuid}/posts`, {state: {allPosts,category,title,topicuid}});
    });
  };
  const submitData = () => {
    createTopicRecord(category, topic)
      .then(topic => {
        createPostToTopicRecord(auth.currentUser.uid, category, topic.id, post)
              .then(() => {
                setShouldRedirect(true);
              })
              .catch(err => {
                console.log(err);
              });
          });
  }

  const createTopicRecord = (category, topic) => {
    return addDoc(collection(db, `/Categories/${category}/topics`), {
      title: topic,
      numOfPosts: 0,
      createDate: serverTimestamp()

    }).then((topic) => {
      setShouldRedirect(true);
      return topic;
    });
  };

  useEffect(() => {

    returnAllCategoryTopics().then(data => setAllTopics(data));
    return () => setShouldRedirect(false);
  }, [shouldRedirect]);
  return (
    <>
      <h2>All topics:</h2>
      {allTopics && <p>Number of topics: {allTopics.length}</p>}
      {allTopics.map(topic => (
        <div className="debug-topic" key={topic.id}>

          <p>{topic.data.createDate.toDate().toLocaleString()}</p>
          <p>category:{topic.category}</p>
          <div className="debug-title" data-category={topic.category} data-id={topic.id} onClick={(e) => {
            returnTopicReplies(e.target.getAttribute('data-id'), e.target.getAttribute('data-category'),topic.data.title);
          }}>{topic.data.title}</div>
        </div>

      ))}


      <div className="debug-form-container">
        <label>
          <span>Choose category:</span>


          <select name="categories" id="cars" value={category} onChange={(e) => {
            setCategory(e.target.value);
          }}>
            <option value="Android">Android</option>
            <option value="iOS">iOS</option>
          </select>


        </label>
        <label>
          <span>Create topic:</span>
          <input type="text" onChange={(e) => setTopic(e.target.value)}/>
        </label>
        <label>
          <p>content:</p>
          <textarea className="debug-textarea" onChange={(e) => setPost(e.target.value)}></textarea>
        </label>
        <div>
          <button onClick={() => submitData()}>Submit
          </button>
        </div>
      </div>


    </>
  );
};