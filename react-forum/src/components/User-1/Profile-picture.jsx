
import React, {useEffect, useState} from 'react';
import {Avatar} from "@mui/material";
import {auth, db} from '../../config/firebase-config';
import {getUserData} from '../../services/users.service.js';

const ProfilePicture = (size) => {
  const userId = sessionStorage.getItem('uid')
  const [loading, setLoading] = useState(false);

  const [userData, setUserData] = useState({});
  // const [name, setName] = useState('')

  // const GetUserData = (uid) => {
  //   return useEffect(()=> {
  //       getUserData(db, '/Users', uid).then(user => setUserData(user.data()))
  //       return () => setLoading(false)
  //     }
  //   ,[loading])
  //
  //
  // };
  //
  // GetUserData(auth.currentUser.uid)

const stringToColor = (string) => {
  let hash = 0;
  let i;

  /* eslint-disable no-bitwise */
  for (i = 0; i < string?.length; i += 1) {
    hash = string.charCodeAt(i) + ((hash << 5) - hash);
  }

  let color = '#';


  for (i = 0; i < 3; i += 1) {
    const value = (hash >> (i * 8)) & 0xff;
    color += `00${value.toString(16)}`.slice(-2);
  }
  /* eslint-enable no-bitwise */

  return color;
};

// const stringAvatar = (username) => {
//   return {
//     sx: {bgcolor: stringToColor(username),},
//     children: `${username.split(' ')[0][0]}${username.split(' ')[1][0]}`,
//   };
// };

const stringAvatar = (username) => {
  return {
    sx: {bgcolor: stringToColor(username), fontSize: size/2, width: size, height: size, border: '1px solid white'},
    children: `${username?.split(' ')[0][0]}`,
  };
};

return (
  // <div>{console.log(typeof userData.userName)}</div>
  // <div>{userData.userName}</div>
  <>
    {/* {console.log(userData)} */}
    {/* <Avatar alt={sessionStorage.getItem('username')} src={sessionStorage.getItem('username')} sx={{ width: 60, height: 60 }} /> */}
    <Avatar {...stringAvatar(sessionStorage.getItem('username'))} />
    {/*{getUserData(auth.currentUser.uid).then(data=> {stringAvatar(userData)})}*/}
    </>
  // <Avatar { }/>
  // <Avatar alt={userData.userName} src='' sx={{ width: 60, height: 60 }} />
  // <Avatar alt='' src='' sx={{ width: 60, height: 60 }} />
);

}

export default ProfilePicture;



// const ProfilePicture = (userid) => {
//   const userId = auth.currentUser.uid;
//   const [loading, setLoading] = useState(false);
//   const [userData, setUserData] = useState({});
//   const [name, setName] = useState('');
//   const [color, setColor] = useState('');
//   const [letters, setLetters] = useState('')
//   const [size, setSize] = useState(50);

//   const GetUserData = (uid) => {
//     return useEffect(()=> {
//         getUserData(db, '/Users', uid)
//         .then(user => {
//           setUserData(user.data())
//           setName((user.data().userName).toUpperCase())
//           setColor(stringToColor(user.data().userName))
//           setLetters()
//         })
//         return () => setLoading(false)
//       }
//     , [loading, uid])
//   };

//   GetUserData(userid)
  

//   const stringToColor = (string) => {
//     let hash = 0;
//     let i;

//     /* eslint-disable no-bitwise */
//     for (i = 0; i < string.length; i += 1) {
//       hash = string.charCodeAt(i) + ((hash << 5) - hash);
//     }

//     let color = '#';

//     for (i = 0; i < 3; i += 1) {
//       const value = (hash >> (i * 8)) & 0xff;
//       color += `00${value.toString(16)}`.slice(-2);
//     }
//     /* eslint-enable no-bitwise */

//     return color;
//   };

//   // const stringAvatar = (username) => {
//   //   return {
//   //     sx: {bgcolor: stringToColor(username),},
//   //     children: `${username.split(' ')[0][0]}${username.split(' ')[1][0]}`,
//   //   };
//   // };
  
//   // setSize(60);

//   return (
//       // <Avatar alt={name} src={loading ? '' : name} sx={{bgcolor: color, border: '1px solid white', width: 60, height: 60 }} />
//     <div>
//       <Avatar alt={loading ? '' : name} src=' ' sx={{bgcolor: color, border: '1px solid white', width: 60, height: 60, fontSize: 60/1.5 }} />
//     </div>
//   );
// }

// export default ProfilePicture;