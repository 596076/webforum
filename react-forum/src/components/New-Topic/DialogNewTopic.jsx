import './DialogNewTopic.css';
import React, {useContext, useState} from 'react';
import CategoriesNewTopic from './Categories-topic';
import TagsNewTopic from './Tags-topic';
import {createPostToTopicRecord} from '../../services/posts.service.js';
import {auth, db} from '../../config/firebase-config.js';
import {addDoc, collection, serverTimestamp} from 'firebase/firestore';
import AppState from '../../Providers/app-state.js';

const PopupCreateTopic = (props) => {
  const [data, setData] = useState({});
  const [category, setCategory] = useState('');
  const [tags, setTags] = useState([]);
  const appCtx = useContext(AppState)
  const submitData = () => {
    createTopicRecord(category, data.title)
      .then(topic => {
        return createPostToTopicRecord(auth.currentUser.uid, category, topic.id, data.content)
          .then(() => {
            setCategory('')
            appCtx.setState({...appCtx.appState,render:false})
            props.setTrigger(false);
          })
          .catch(err => {
            console.log(err);
          });
      });

  };

  const createTopicRecord = (category, topic) => {
    return addDoc(collection(db, `/Categories/${category}/topics`), {
      title: topic,
      numOfPosts: 0,
      createDate: serverTimestamp(),
      lastEdit: serverTimestamp(),
      tags: tags

    }).then((topic) => {
      // setShouldRedirect(true);
      return topic;
    });
  };

  return (props.trigger)
    ? (
      <div className="popup-ct">
        <div className="popup-ct-inner">
          {/* <form className='popup-ct-inner'> */}
          <input className="topic-title" type="text" placeholder="Title..." onChange={(e) =>
            setData({...data, title: e.target.value})}/><br/>
          <div className="topic-category-tag">
            <CategoriesNewTopic setCategory={setCategory}/>
            <TagsNewTopic setTags={setTags}/>
            {/* <input className='topic-category' type='text' placeholder='category'></input>
                  <input className='topic-tag' type='text' placeholder='tags'></input> */}
          </div>
          <textarea className="topic-content" type="text" placeholder="Type here..." onChange={(e) =>
            setData({...data, content: e.target.value})}/>
          {props.children}
          <div className="buttons">
            <button className="submit" onClick={() => {
              submitData();
            }}>Submit
            </button>
            <button className="cancel" onClick={() => props.setTrigger(false)}>Cancel</button>
          </div>
          {/* </form> */}
        </div>
      </div>
    )
    : '';
};

export default PopupCreateTopic;