import React, { useState } from 'react';
import './Tags.css';
// import Creatable from 'react-select/creatable';
import Select from 'react-select';

// import { defaultStyles } from 'react-select/dist/declarations/src/styles';


//TODO - use AppState to protect the page!

  const tags = [
    {label: 'Tag 1', value: 1},
    {label: 'Tag 2', value: 2},
    {label: 'Tag 3', value: 3},
    {label: 'Tag 4', value: 4},
    {label: 'Tag 5', value: 5},
    {label: 'Tag 6', value: 6},
    {label: 'Tag 7', value: 7},
    {label: 'Tag 8', value: 8},
    {label: 'Tag 9', value: 9},
  ];

  const customStyles = {
    option: (provided, state) => ({
      ...provided,
      borderBottom: '1px solid #d4d4d4',
      // color: state.isSelected ? 'red' : '#0078aa',
      color: '#0078aa',
      padding: 10,
    }),

    control: (style, state) => ({
      ...style,
      borderRadius: 22,
      // borderColor: '#d4d4d4',
      borderColor: state.isSelected ? '#f6f6f6' : '#d4d4d4',
      background: state.isFocused ? '#f6f6f6' : '#0078aa',
      color: '#d4d4d4',
      justify: 'top',
      // with: state.isFocused ? '300px' : '180px',
      with: '180px',
    }),
      
    menu: styles => ({
      ...styles,
      width: '370px',
    }),

    input: (style, state) => ({
      ...style,
      color: '#0078aa',
      
    }),

    // multiValue: (style) => ({
    //   ...style,
    //   // background: 'red',
    //   display: 'flex',
    //   // margin: '0px',
    // }),

    multiValueLabel: (style) => ({
      ...style,
      color: '#0078aa',
      outerHeight: 5,
      height: '20px',
      padding: '0 10px',
      
    }),

    multiValueRemove: (style) => ({
      ...style,
      background: '#d4d4d4',
      color: '#0078aa',
      height: '20px',
      ali: 'center',
    }),

    // singleValue: (provided, state) => {
    //   const opacity = state.isDisabled ? 0.5 : 1;
    //   const transition = 'opacity 300ms';

    //   return { ...provided, opacity, transition };
    // },

    placeholder: (styles, state) => ({
        ...styles,
        color: state.isFocused ? '#0078aa' : '#d4d4d4',
    })
  };

  const Tags = (props) => {
    const [tagValue, setTagValue] = useState('');
    // const [tagInputValue, setTagInputValue] = useState('');
    // const [tagValue, setTagValue] = useState('');
  
    const handleChange = (field, value) => {
      switch (field) {
        case 'tags':
          setTagValue(value);
          break;
      
        default:
          break;
      }
    }

    // const handleKeyDown = event => {
    //   if (!tagInputValue) return;

    //   switch (event.key) {
    //     case 'Enter':
    //     case 'Tab':
    //       setTagValue([...tagValue, createOption(tagInputValue)]);
    //       setTagInputValue('');

    //       event.preventDefault();
    //       break;
      
    //     default:
    //       break;
    //   }
    // }

    // const createOption = label => ({
    //   label,
    //   value: label
    // })

    // const handleInputChange = (value) => {
    //   setTagInputValue(value);
    // }

    return (
      <div className='tag-container'>
        <div className='tag-form'>
          <div className='tag-input'>
            <Select
              // color='blue'
              placeholder='Tag(s)...'
              isMulti
              onChange={(value) => handleChange('tags', value)}
              options={tags}
              value={tagValue}
              styles={customStyles}
            />
            {/* <Creatable
              placeholder='Tag'
              isClearable
              isMulti
              components={
                {DropdownIndicator: null}
              }
              inputValue={tagInputValue}
              menuIsOpen={false}
              onChange={(value) => handleChange('tags', value)}
              onKeyDown={handleKeyDown}
              onInputChange={handleInputChange}
              value={tagValue}
              styles={customStyles}
            /> */}
          </div>
        </div>
      </div>
    )
  }

export default Tags;








// import * as React from 'react';
// import TextField from '@mui/material/TextField';
// import Autocomplete from '@mui/material/Autocomplete';
// import CircularProgress from '@mui/material/CircularProgress';


// function sleep(delay = 0) {
//   return new Promise((resolve) => {
//     setTimeout(resolve, delay);
//   });
// }

// const Tags = () => {
//   const [open, setOpen] = React.useState(false);
//   const [options, setOptions] = React.useState([]);
//   const loading = open && options.length === 0;

//   React.useEffect(() => {
//     let active = true;

//     if (!loading) {
//       return undefined;
//     }

//     (async () => {
//       await sleep(100); // For demo purposes.

//       if (active) {
//         setOptions([...topFilms]);
//       }
//     })();

//     return () => {
//       active = false;
//     };
//   }, [loading]);

//   React.useEffect(() => {
//     if (!open) {
//       setOptions([]);
//     }
//   }, [open]);

//   return (
//     <Autocomplete
//       size='small'
//       id="asynchronous-demo"
//       sx={{ width: 220 }}
//       open={open}
//       onOpen={() => {
//         setOpen(true);
//       }}
//       onClose={() => {
//         setOpen(false);
//       }}
//       isOptionEqualToValue={(option, value) => option.title === value.title}
//       getOptionLabel={(option) => option.title}
//       options={options}
//       loading={loading}
//       renderInput={(params) => (
//         <TextField
//           {...params}
//           label="Tag"
//           InputProps={{
//             ...params.InputProps,
//             endAdornment: (
//               <React.Fragment>
//                 {loading ? <CircularProgress color="inherit" size={20} /> : null}
//                 {params.InputProps.endAdornment}
//               </React.Fragment>
//             ),
//           }}
//         />
//       )}
//     />
//   );
// }

// // Top films as rated by IMDb users. http://www.imdb.com/chart/top
// const topFilms = [
//   { title: 'The Shawshank Redemption', year: 1994 },
//   { title: 'The Godfather', year: 1972 },
//   { title: 'The Godfather: Part II', year: 1974 },
//   { title: 'The Dark Knight', year: 2008 },
//   { title: '12 Angry Men', year: 1957 },
//   { title: "Schindler's List", year: 1993 },
//   { title: 'Pulp Fiction', year: 1994 },
//   {
//     title: 'The Lord of the Rings: The Return of the King',
//     year: 2003,
//   },
//   { title: 'The Good, the Bad and the Ugly', year: 1966 },
//   { title: 'Fight Club', year: 1999 },
//   {
//     title: 'The Lord of the Rings: The Fellowship of the Ring',
//     year: 2001,
//   },
//   {
//     title: 'Star Wars: Episode V - The Empire Strikes Back',
//     year: 1980,
//   },
//   { title: 'Forrest Gump', year: 1994 },
//   { title: 'Inception', year: 2010 },
//   {
//     title: 'The Lord of the Rings: The Two Towers',
//     year: 2002,
//   },
//   { title: "One Flew Over the Cuckoo's Nest", year: 1975 },
//   { title: 'Goodfellas', year: 1990 },
//   { title: 'The Matrix', year: 1999 },
//   { title: 'Seven Samurai', year: 1954 },
//   {
//     title: 'Star Wars: Episode IV - A New Hope',
//     year: 1977,
//   },
//   { title: 'City of God', year: 2002 },
//   { title: 'Se7en', year: 1995 },
//   { title: 'The Silence of the Lambs', year: 1991 },
//   { title: "It's a Wonderful Life", year: 1946 },
//   { title: 'Life Is Beautiful', year: 1997 },
//   { title: 'The Usual Suspects', year: 1995 },
//   { title: 'Léon: The Professional', year: 1994 },
//   { title: 'Spirited Away', year: 2001 },
//   { title: 'Saving Private Ryan', year: 1998 },
//   { title: 'Once Upon a Time in the West', year: 1968 },
//   { title: 'American History X', year: 1998 },
//   { title: 'Interstellar', year: 2014 },
// ];


// export default Tags;





// import * as React from 'react';
// import TextField from '@mui/material/TextField';
// import Autocomplete from '@mui/material/Autocomplete';
// import Stack from '@mui/material/Stack';

// export default function Playground() {
//   const defaultProps = {
//     options: top100Films,
//     getOptionLabel: (option) => option.title,
//   };

//   // const flatProps = {
//   //   options: top100Films.map((option) => option.title),
//   // };

//   // const [value, setValue] = React.useState(null);

//   return (
//     <Stack spacing={1} sx={{ typography: '10', width: 200, marginBottom: 1, color: 'white' }}>

//       <Autocomplete
//         {...defaultProps}
//         id="clear-on-escape"
//         clearOnEscape
//         renderInput={(params) => (
//           <TextField {...params} label="Tags" variant="standard" />
//         )}
//       />

//     </Stack>
//   );
// }

// // Top 100 films as rated by IMDb users. http://www.imdb.com/chart/top
// const top100Films = [
//   { title: 'The Shawshank Redemption', year: 1994 },
//   { title: 'The Godfather', year: 1972 },
//   { title: 'The Godfather: Part II', year: 1974 },
//   { title: 'The Dark Knight', year: 2008 },
//   { title: '12 Angry Men', year: 1957 },
//   { title: "Schindler's List", year: 1993 },
//   { title: 'Pulp Fiction', year: 1994 },
//   {
//     title: 'The Lord of the Rings: The Return of the King',
//     year: 2003,
//   },
//   { title: 'The Good, the Bad and the Ugly', year: 1966 },
//   { title: 'Fight Club', year: 1999 },
//   {
//     title: 'The Lord of the Rings: The Fellowship of the Ring',
//     year: 2001,
//   },
//   {
//     title: 'Star Wars: Episode V - The Empire Strikes Back',
//     year: 1980,
//   },
//   { title: 'Forrest Gump', year: 1994 },
//   { title: 'Inception', year: 2010 },
//   {
//     title: 'The Lord of the Rings: The Two Towers',
//     year: 2002,
//   },
//   { title: "One Flew Over the Cuckoo's Nest", year: 1975 },
//   { title: 'Goodfellas', year: 1990 },
//   { title: 'The Matrix', year: 1999 },
//   { title: 'Seven Samurai', year: 1954 },
//   {
//     title: 'Star Wars: Episode IV - A New Hope',
//     year: 1977,
//   },
//   { title: 'City of God', year: 2002 },
//   { title: 'Se7en', year: 1995 },
//   { title: 'The Silence of the Lambs', year: 1991 },
//   { title: "It's a Wonderful Life", year: 1946 },
//   { title: 'Life Is Beautiful', year: 1997 },
//   { title: 'The Usual Suspects', year: 1995 },
//   { title: 'Léon: The Professional', year: 1994 },
//   { title: 'Spirited Away', year: 2001 },
//   { title: 'Saving Private Ryan', year: 1998 },
//   { title: 'Once Upon a Time in the West', year: 1968 },
//   { title: 'American History X', year: 1998 },
//   { title: 'Interstellar', year: 2014 },
//   { title: 'Casablanca', year: 1942 },
//   { title: 'City Lights', year: 1931 },
//   { title: 'Psycho', year: 1960 },
//   { title: 'The Green Mile', year: 1999 },
//   { title: 'The Intouchables', year: 2011 },
//   { title: 'Modern Times', year: 1936 },
//   { title: 'Raiders of the Lost Ark', year: 1981 },
//   { title: 'Rear Window', year: 1954 },
//   { title: 'The Pianist', year: 2002 },
//   { title: 'The Departed', year: 2006 },
//   { title: 'Terminator 2: Judgment Day', year: 1991 },
//   { title: 'Back to the Future', year: 1985 },
//   { title: 'Whiplash', year: 2014 },
//   { title: 'Gladiator', year: 2000 },
//   { title: 'Memento', year: 2000 },
//   { title: 'The Prestige', year: 2006 },
//   { title: 'The Lion King', year: 1994 },
//   { title: 'Apocalypse Now', year: 1979 },
//   { title: 'Alien', year: 1979 },
//   { title: 'Sunset Boulevard', year: 1950 },
//   {
//     title: 'Dr. Strangelove or: How I Learned to Stop Worrying and Love the Bomb',
//     year: 1964,
//   },
//   { title: 'The Great Dictator', year: 1940 },
//   { title: 'Cinema Paradiso', year: 1988 },
//   { title: 'The Lives of Others', year: 2006 },
//   { title: 'Grave of the Fireflies', year: 1988 },
//   { title: 'Paths of Glory', year: 1957 },
//   { title: 'Django Unchained', year: 2012 },
//   { title: 'The Shining', year: 1980 },
//   { title: 'WALL·E', year: 2008 },
//   { title: 'American Beauty', year: 1999 },
//   { title: 'The Dark Knight Rises', year: 2012 },
//   { title: 'Princess Mononoke', year: 1997 },
//   { title: 'Aliens', year: 1986 },
//   { title: 'Oldboy', year: 2003 },
//   { title: 'Once Upon a Time in America', year: 1984 },
//   { title: 'Witness for the Prosecution', year: 1957 },
//   { title: 'Das Boot', year: 1981 },
//   { title: 'Citizen Kane', year: 1941 },
//   { title: 'North by Northwest', year: 1959 },
//   { title: 'Vertigo', year: 1958 },
//   {
//     title: 'Star Wars: Episode VI - Return of the Jedi',
//     year: 1983,
//   },
//   { title: 'Reservoir Dogs', year: 1992 },
//   { title: 'Braveheart', year: 1995 },
//   { title: 'M', year: 1931 },
//   { title: 'Requiem for a Dream', year: 2000 },
//   { title: 'Amélie', year: 2001 },
//   { title: 'A Clockwork Orange', year: 1971 },
//   { title: 'Like Stars on Earth', year: 2007 },
//   { title: 'Taxi Driver', year: 1976 },
//   { title: 'Lawrence of Arabia', year: 1962 },
//   { title: 'Double Indemnity', year: 1944 },
//   {
//     title: 'Eternal Sunshine of the Spotless Mind',
//     year: 2004,
//   },
//   { title: 'Amadeus', year: 1984 },
//   { title: 'To Kill a Mockingbird', year: 1962 },
//   { title: 'Toy Story 3', year: 2010 },
//   { title: 'Logan', year: 2017 },
//   { title: 'Full Metal Jacket', year: 1987 },
//   { title: 'Dangal', year: 2016 },
//   { title: 'The Sting', year: 1973 },
//   { title: '2001: A Space Odyssey', year: 1968 },
//   { title: "Singin' in the Rain", year: 1952 },
//   { title: 'Toy Story', year: 1995 },
//   { title: 'Bicycle Thieves', year: 1948 },
//   { title: 'The Kid', year: 1921 },
//   { title: 'Inglourious Basterds', year: 2009 },
//   { title: 'Snatch', year: 2000 },
//   { title: '3 Idiots', year: 2009 },
//   { title: 'Monty Python and the Holy Grail', year: 1975 },
// ];









// const Tags = () => {
//   return (
//     <div>
//       <h1 style={{textAlign: 'center'}}>Tags</h1>
//     </div>
//   )
// }

// export default Tags;