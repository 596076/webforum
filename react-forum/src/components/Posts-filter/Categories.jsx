import React, { useState } from 'react';
import './Categories.css';
import Select from 'react-select';
// import { defaultStyles } from 'react-select/dist/declarations/src/styles';


//TODO - use AppState to protect the page!

  const categories = [
    {label: 'Category 1', value: 1},
    {label: 'Category 2', value: 2},
    {label: 'Category 3', value: 3},
    {label: 'Category 4', value: 4},
    {label: 'Category 5', value: 5},
    {label: 'Category 6', value: 6},
    {label: 'Category 7', value: 7},
    {label: 'Category 8', value: 8},
    {label: 'Category 9', value: 9},
  ];

  // const indicatorSeparatorStyle = {
  //   alignSelf: 'stretch',
  //   backgroundColor: colourOptions[2].color,
  //   marginBottom: 8,
  //   marginTop: 8,
  //   width: 1,
  // };

  // const IndicatorSeparator = ({
  //   innerProps,
  // }: IndicatorSeparatorProps<ColourOption, true>) => {
  //   return <span style={indicatorSeparatorStyle} {...innerProps} />;
  // };

  const customStyles = {
    option: (provided, state) => ({
      ...provided,
      borderBottom: '1px solid #d4d4d4',
      // color: 'white',
      color: state.isSelected ? 'white' : '#0078aa',
      background: state.isSelected ? '#0078aa' : '',
      // color: '#0078aa',
      padding: 10,
    }),

    control: (style, state) => ({
      ...style,
      borderRadius: 22,
      // borderColor: '#d4d4d4',
      borderColor: state.isSelected ? '#f6f6f6' : '#d4d4d4',
      background: state.isFocused ? '#f6f6f6' : '#0078aa',
      // color: '#d4d4d4',
      color: state.isFocused ? 'red' : 'blue',
    }),

    singleValue: (style, state) => ({
      ...style,
      color: state.isSelected ? 'red' : 'white',
      background: '#0078aa',
    }),

    valueContainer: (provided) => ({
      ...provided,
      color: 'red'
    }),
      
    menu: styles => ({
      ...styles,
      width: '370px',
    }),

    input: style => ({
      ...style,
      color: '#0078aa',
    }),

    // singleValue: (provided, state) => {
    //   const opacity = state.isDisabled ? 0.5 : 1;
    //   const transition = 'opacity 300ms';

    //   return { ...provided, opacity, transition };
    // },

    placeholder: (styles, state) => ({
        ...styles,
        color: state.isFocused ? '#0078aa' : '#d4d4d4',
        // color: state.isDisabled ? 'white' : state.isFocused ? '#0078aa' : '#d4d4d4',
        // background: state.isSelected ? 'white' : '',
    })
  };

  const Categories = (props) => {
    const [tagValue, setTagValue] = useState('');
  
    const handleChange = (field, value) => {
      switch (field) {
        case 'categories':
          setTagValue(value);
          break;
      
        default:
          break;
      }
    }

    return (
      <div className='category-container'>
        <div className='category-form'>
          <div className='category-input'>
            <Select
              placeholder='Categories...'
              isClearable
              // closeMenuOnSelect={false}
              onChange={(value) => handleChange('categories', value)}
              // components={{ IndicatorSeparator }}
              defaultValue={categories[0]}
              // isMulti
              options={categories}
              value={tagValue}
              styles={customStyles}
            />
            {/* <Creatable
              placeholder='Tag(s)...'
              isClearable
              isMulti
              onChange={(value) => handleChange('tags', value)}
              options={tags}
              value={tagValue}
              styles={customStyles}
            /> */}
          </div>
        </div>
      </div>
    )
  }

export default Categories;
