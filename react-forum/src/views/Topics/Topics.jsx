import "./Topics.css";
import {useEffect, useState} from 'react';
import {returnAllCategoryTopics} from '../../services/topics.service.js';
import {collection, getDocs, orderBy, query} from 'firebase/firestore';
import {db} from '../../config/firebase-config.js';
import {useNavigate,Link} from 'react-router-dom';

export const Topics = () => {
  const [allTopics, setAllTopics] = useState([]);
  const [shouldRedirect, setShouldRedirect] = useState(false);

  useEffect(() => {

    returnAllCategoryTopics().then(data => setAllTopics(data));
    return () => setShouldRedirect(false);
  }, [shouldRedirect]);


  return (
    <div className="Topics-container">
      <h2>All topics:</h2>
      {allTopics && <p>Number of topics: {allTopics.length}</p>}
      {allTopics.map(topic => (
        <div className="debug-topic" key={topic.id}>

          <p>{topic.data.createDate.toDate().toLocaleString()}</p>
          <p>category:{topic.category}</p>
          <div className="debug-title"><Link to={`/start/debug/${topic.category}/${topic.id}/posts`}>{topic.data.title}</Link></div>
        </div>

      ))}

    </div>
  );
};