import './Login.scss';
// import { auth } from '../../config/firebase-config';
import React, {useContext, useEffect, useState} from 'react';
import {Link, useLocation, useNavigate} from 'react-router-dom';
import AppState from '../../Providers/app-state';
import {db} from '../../config/firebase-config'
import {signInUserWithEmail, getUserData} from '../../services/users.service.js';
import {toast} from 'react-toastify';
import {returnAllCategoryTopics} from '../../services/topics.service.js';
const Login = () => {
  const navigate = useNavigate();
  const { setState } = useContext(AppState);
 
  const location = useLocation();
  const [email, setEmail] = useState('');
  // const [emailError, setEmailError] = useState(false);
  // const [passwordError, setPasswordError] = useState(false);
  const [password, setPassword] = useState('');

  const [success, setSuccess] = useState(false);
  const [message, setMessage] = useState(null);
  const showMessage = (msg) => {
    return toast.error(msg, {
      position: "top-left",
      autoClose: 5000,
      hideProgressBar: true,
      closeOnClick: true,
      pauseOnHover: false,
      draggable: true,
      progress: undefined,
    });
  };

  const login = () => {
    signInUserWithEmail(email, password)
    .then(user => {
      if (user.isAnonymous === false) {
        
        getUserData(db, '/Users', user?.uid)
        .then(resp => {
          setState({user: user,
            userData: {...resp.data()}})
          returnAllCategoryTopics().then(topics=>{

          })
        })

        .catch((error) => 
        console.log(error.message));
        console.log(user)
        for(let [k,v] of Object.entries({
          uid: user.uid,
          email: user.email,
          isAnonymous: user.isAnonymous,
          createdAt: user.createdAt,
          lastLoginAt: user.lastLoginAt,
          username: user.providerData[0].displayName || 'Unknown Name'

        })){sessionStorage.setItem(k,v)}

        navigate(location?.state?.from ?? '/home', {replace: true});
      } else {
        console.log(success)
        if (user.code === 'auth/wrong-password' || user.code === 'auth/user-not-found') {
          setSuccess(true);
          setMessage('Wrong username/password');
        }else{
          setSuccess(true);
          setMessage('Authentication error');
        }
      }
    }).catch(err => console.log(err));
  };


  const handleLogin = (e) => {
    e.preventDefault();
  }

  return (
    <div className='login'>

      {useEffect(() => {
         success && showMessage(message);
          return () => setSuccess(false);
      }, [success, message])} 

      <form onSubmit={handleLogin}>
        <h2>Log In</h2>
        <hr className="new1"></hr>
        <input type='email' placeholder='email'
          onChange={(e) => setEmail(e.target.value)} />
        <input type='password' placeholder='password'
          onChange={(e) => setPassword(e.target.value)}
          onKeyDown={(e)=> {e.key === 'Enter' && login()}} />
        {/* {error && <span>Wrong email or password!</span>} */}
        <button onClick={login}>Log In</button>
        <hr className="new2"></hr>
          <div className='not-a-member'>
            <p style={{marginRight: "15px"}}>Not a member?</p>
            <p><Link to="/register" className='login-signup'>Sign Up</Link></p>
          </div>
      </form>
    </div>
  )
};

export default Login;

//------------------------------

// // TODO Sign pu Form! (Switch between Login and Sign up)

// const Login = () => {
//   return (
//     <div className="login-form-container">

//       <div className="login-form">
//         <h1>Login</h1>
//         <hr/>
//         <FormControl error={emailError} variant="standard">
//           <InputLabel htmlFor="component-simple">email</InputLabel>
//           <Input id="component-simple"
//                  value={email}
//                  aria-describedby="component-error-text"
//                  onBlur={(e) => {
//                    /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/.test(e.target.value) ? setEmailError(false) : setEmailError(true);
//                  }}
//                  onChange={(e) => setEmail(e.target.value)}/>
//           <FormHelperText id="component-error-text">{emailError && 'Bad email'}</FormHelperText>
//         </FormControl>

//         <FormControl error={passwordError} variant="standard">
//           <InputLabel htmlFor="component-password">Password</InputLabel>
//           {/*<InputLabel htmlFor="component-simple">password</InputLabel>*/}
//           <Input id="component-password"
//                  type="password"
//                  value={password}
//                  aria-describedby="component-error-text"
//                  onBlur={(e) => {
//                    e.target.value.length > 6 ? setPasswordError(false) : setPasswordError(true);
//                  }}
//                  onChange={(e) => setPassword(e.target.value)}
//                  onKeyDown={(e)=> e.key == 'Enter' && login()}
//           />
//           <FormHelperText id="component-error-text">{passwordError && 'Password too short'}</FormHelperText>
//         </FormControl>
//         {/*</Box>*/}
//         <Button onClick={login} variant="contained" endIcon={<LoginIcon/>} style={{marginTop: "20px"}}>
//           Login
//         </Button>
//         <p style={{marginTop: "20px"}}>Not a member? <Link to="/register">Signup</Link></p>
//       </div>

//     </div>
//   );
// };

// export default Login;
