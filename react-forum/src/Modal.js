// import React from 'react';
// import { Dialog, DialogTitle, IconButton } from '@mui/material';
// import { Close } from '@mui/icons-material';
// import { useAuth } from './components/context/AuthContext';

// const Modal = () => {
//   const [modal, setModal] = useAuth();
//   const handleClose = () => {
//     setModal({ ...modal, isOpen: false });
//   }

//   return (
//     <Dialog open={modal.isOpen} onClose={handleClose}>
//       <DialogTitle>
//         {modal.title}
//         <IconButton
//           aria-label='Close'
//           onClick={handleClose}
//           sx={{
//             position: 'absolute',
//             top: 8,
//             right: 8,
//             color: (theme) => theme.palette.grey[500]
//           }}>
//           <Close />
//         </IconButton>
//       </DialogTitle>
//       {modal.content}
//     </Dialog>
//   )
// }

// export default Modal;