import {useNavigate, useParams} from 'react-router-dom';
import "./DebugPosts.css";
import {Fragment, useEffect, useState} from 'react';
import {createPostToTopicRecord, returnAllPostsFromTopic} from '../../services/posts.service.js';
import {getTopicData} from '../../services/topics.service.js';
import {CKEditor} from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import {auth} from '../../config/firebase-config.js';
import parse from 'html-react-parser';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faHandPointLeft } from '@fortawesome/free-solid-svg-icons'
import { faHeart} from '@fortawesome/free-regular-svg-icons';
import { faHeart as faHeartSolid} from '@fortawesome/free-solid-svg-icons';
import {Post} from './Post.js';
export const DebugPosts = () => {
  let {topicuid: topicid, category} = useParams();
  // category = category.toLowerCase() === 'android' ? 'Android' : 'iOS';
  const [posts, setPosts] = useState([]);
  const [rerender, setShouldRerender] = useState(false);
  const [topic, setTopic] = useState({});
  const [newPost, setNewPost] = useState('');
  const navigate = useNavigate()
  useEffect(() => {
    getTopicData(category, topicid).then(data => {
      setTopic(data.data());
    });

  }, []);



  const saveData = () => {
    createPostToTopicRecord(auth.currentUser.uid, category, topicid, newPost).then(() => setShouldRerender(true));
  };
  useEffect(() => {
    returnAllPostsFromTopic(category, topicid).then(posts => {
      const dataPosts = [];
      posts.forEach(post => dataPosts.push(post));
      setPosts(dataPosts);
    });
    return () => setShouldRerender(false);
  }, [rerender]);
  return (

    <div className="debug-post-container">
      <h1>{topic.title}</h1>
      {/*<p>{state.topicuid}</p>*/}
      <hr/>
      <span onClick={()=>navigate(-1)}><FontAwesomeIcon icon={faHandPointLeft} style={{marginLeft:"20px"}}/></span>
      <div>
        {posts.sort( (objA, objB) => Number(objA.createDate) - Number(objB.createDate)).map((post, index) => (
          <Fragment key={post.id}>
            <Post index={index} post={post} category={category} />

          </Fragment>
          ))
        }
      </div>
      { sessionStorage.getItem('isAnonymous') &&
        (<><CKEditor
          editor={ClassicEditor}
          onChange={(e, editor) => {
            setNewPost(editor.getData());
          }}
        >

        </CKEditor>
        <button className="post-button" onClick={() => saveData()}>Save Post</button>
        </>)
      }
    </div>

  );
};
