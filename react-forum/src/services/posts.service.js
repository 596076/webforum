import {addDoc, collection, getDocs, serverTimestamp} from 'firebase/firestore';
import {db} from '../config/firebase-config.js';
import {updateTopicCounter} from './topics.service.js';

export const createPostToTopicRecord = (uid, category, topicuid, content) => {
  return addDoc(collection(db, `/Categories/${category}/topics/${topicuid}/posts`), {
    content: content,
    createDate: serverTimestamp(),
    likes: 0,
    user_id: uid,
    createdBy: sessionStorage.getItem('username')

  }).then(()=>{
    updateTopicCounter(category,topicuid)
  }).catch(err=>console.log(err.message))
}


export const returnAllPostsFromTopic = (category,topicuid) =>{
  return getDocs(collection(db,`/Categories/${category}/topics/${topicuid}/posts`))
}