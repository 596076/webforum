import {HiOutlineEmojiSad} from 'react-icons/hi'
import './404.css'

export const Error404 = () => (
    <div className="error-container">
      <HiOutlineEmojiSad style={{width:"300px",height:"300px"}}/>
      <h1>404</h1>
      <h2>Page not found</h2>
      <p>The page you are looking for doesn't exist.</p>
      <p>Go back, or head over to home page to choose a new direction.</p>
  </div>
)


