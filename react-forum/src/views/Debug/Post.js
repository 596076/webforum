import parse from 'html-react-parser';
import {useState} from 'react';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faHeart} from '@fortawesome/free-regular-svg-icons';
import {faHeart as faHeartSolid} from '@fortawesome/free-solid-svg-icons';

export const Post=({index,post,category})=>{
  const [like,setLike] = useState(false)

  const likeBtn = () =>{
    return (like?<FontAwesomeIcon icon={ faHeartSolid} style={{color:"green"}} onClick={()=>setLike(!like)}/>:
      <FontAwesomeIcon icon={faHeart} style={{color:"green"}} onClick={()=>setLike(!like)}  />)
  }

  const sendLikeData = () =>{

  }
  return (

        <div key={post.id} className="debug-post">
          <div className="post-number">
            {index + 1}
          </div>
          <div>
            <p>Author: {post.data().createdBy || "Johnny B."}</p>
            <p> Like it: {likeBtn()} </p>
            <p> category: {category}</p>
            <p> created : {post.data().createDate.toDate().toLocaleString()}</p>
            <div>{parse(post.data().content)}</div>
          </div>
        </div>
      )
}
