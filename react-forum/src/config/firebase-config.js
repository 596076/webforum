import { initializeApp } from "firebase/app";
import { getStorage } from 'firebase/storage';
import { getFirestore } from 'firebase/firestore';
import { getAuth } from 'firebase/auth';

// web-forum

// const firebaseConfig = {
//   apiKey: "AIzaSyAWAjnVVwM1f2F4pvp3TIFW02kA5rE7IGk",
//   authDomain: "web-forum-7805a.firebaseapp.com",
//   databaseURL: "https://web-forum-7805a-default-rtdb.europe-west1.firebasedatabase.app",
//   projectId: "web-forum-7805a",
//   storageBucket: "web-forum-7805a.appspot.com",
//   messagingSenderId: "374345192123",
//   appId: "1:374345192123:web:a96fec0f025621bf8a0f9e"
// };

// web-forum2

const firebaseConfig = {
  apiKey: "AIzaSyBcQKU0YsInNPopLOxm8GPdHLLqKc3r5wE",
  authDomain: "react-forum-a1be6.firebaseapp.com",
  databaseURL: "https://react-forum-a1be6-default-rtdb.europe-west1.firebasedatabase.app",
  projectId: "react-forum-a1be6",
  storageBucket: "react-forum-a1be6.appspot.com",
  messagingSenderId: "555696005924",
  appId: "1:555696005924:web:38ce2c25457c7ed03389b1"
};


export const app = initializeApp(firebaseConfig);
export const storage = getStorage();
export const db = getFirestore(app);
export const auth = getAuth(app);

