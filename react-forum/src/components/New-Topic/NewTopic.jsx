import "./NewTopic.css";
import { Tooltip } from '@mui/material';
import React, { useContext, useState } from 'react';
import AppState from '../../Providers/app-state';
import PopupCreateTopic from "./DialogNewTopic";

const NewTopic = () => {
  const { appState } =useContext(AppState);
  const userSession = sessionStorage.getItem('username');
  const [buttonPopupCT, setButtonPopupCT] = useState(false);

  return (
    <div>
      {userSession
        ? <div className="new-topic-place">
            <Tooltip
              title="Add New Topic"
              arrow
              componentsProps={{
                tooltip: {
                  sx: {
                    marginTop: '10px !important',
                    bgcolor: '#0078aa',
                    fontFamily: 'Poppins',
                    fontSize: 17,
                    '& .MuiTooltip-arrow': {
                      color: '#0078aa',
                    },
                  },
                },
              }}
            >
            <div className="new-topic" onClick={() => setButtonPopupCT(true)}><i className="fa-solid fa-circle-plus"></i></div>
            </Tooltip>
          </div>
        : null
      }
          <PopupCreateTopic trigger={buttonPopupCT} setTrigger={setButtonPopupCT}>
            
          </PopupCreateTopic>
    </div>
  )
}

export default NewTopic;