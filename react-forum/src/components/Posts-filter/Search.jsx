import './Search.css';
import React, { useContext } from 'react';
import AppState from '../../Providers/app-state';

//TODO - use AppState to protect the page!

const Search = () => {
  // const { appState } = useContext(AppState);
  // const user = appState.user;
  const userSession = sessionStorage.getItem('username');

  
  return (
    <div>
      {userSession
        ? <div className="search-box">
            <input type="search" placeholder="Search..."/>  
          </div>
        : null
      }
    </div>
  )
}

export default Search;