// write data; async operation
import {
  addDoc,
  collection,
  deleteDoc,
  deleteField,
  doc,
  getDoc,
  getDocs,
  limit,
  orderBy,
  query,
  serverTimestamp,
  setDoc,
  updateDoc,
  // collectionGroup,
  where
} from 'firebase/firestore';
import { createUserWithEmailAndPassword, signInWithEmailAndPassword } from "firebase/auth";
import { auth, db } from '../config/firebase-config';
import { userRole } from '../common/user-role'

// get All data from collection ; async operation
// export const getAllCollectionData = async (db, col) => {
//   const querySnapshot = await getDocs(collection(db, col));
//   querySnapshot.forEach((doc) => {
//     console.log(`${doc.id} => ${doc.data()}`);
//   });
// };

//*********** Update user data **********


// ********** Signin user **********
export const signInUserWithEmail = (email, password) => {
  return signInWithEmailAndPassword(auth, email, password)
    .then((userCredential) => {
      return userCredential.user;

    })
    .catch((error) => {
      return error
    });
}

// ********** create user *********
// return user credentail
export const createUser = ({ username, email, password }) => {

  return createUserWithEmailAndPassword(auth, email, password)
    .then((userCredential) => {
      // Signed in
      return userCredential
    })
  // .catch ((error) => {
  //   const errorCode = error.code;
  //   const errorMessage = error.message;
  // })
}

// ************** ADD & UPDATE data ***********

export const addDocument = async (db, col, data) => await addDoc(collection(db, col), data);

// create or update document if exists; if { merge: true } is passed data will be merged
export const updateUserById = async (db, col, uid, dataDoc) => await setDoc(doc(db, col, uid), dataDoc, { merge: true });

// add user with random generated id and returns  reference to newly created user document
export const addUser = async (db, col, userData) => {
  const userRef = await addDoc(collection(db, col), userData);
  return userRef;
};

// Update specific fields in user document and set updateTime <-- //TODO try and fix
// If document contains nested objects, you can use "dot notation" to reference nested fields
// {
//     "age": 13,
//     "favorites.color": "Red"
// }    <- Very IMPORTANT during update
export const updateUserDocumentField = async (db, col, docId, userData, value) => {
  const userRef = doc(db, col, docId);
  await updateDoc(userRef, userData, value );
};

// ******* DELETE data ******
// Delete user by id
export const deleteUserById = async (db, col, userId) => {
  await deleteDoc(doc(db, col, userId));
};

// Delete specific field from document,pass array of elements to delete
export const deleteUserDocumentField = async (db, col, userId, arr) => {
  const getUserRef = getUserRefById(db, col, userId);
  await updateDoc(getUserRef, arr.reduce((acc, cur) => {
    acc = { ...acc, cur: deleteField() }
    return acc
  }, {}))
};
// *****************  GET data ************
// return document reference
export const getUserRefById = (db, col, uid) => {
  return doc(db, col, uid);
};

// return document reference path approach :) don't like it
export const getDocumentByIdPath = (db, col, uid) => {
  return doc(db, `${col}/${uid}`);
};

// Get id from auth



// Returns all usernames to check

export const getUserByUsername = async (username) => {
  const q = query(collection(db, '/Users'), where('username', '==', username));
  const querySnapshot = await getDocs(q);
  return querySnapshot || null;
}

export const getUserById = async (userId) => {
  const docRef = doc(db, '/Users', userId);
  const querySnapshot = await getDoc(docRef);
  return querySnapshot || null;
}

// Create User

export const createUserUsername = (userName, uid, email) => {
  setDoc(doc(db, `/Users`, uid), {
    uid, userName, email, firstName: '', lastName: '', createData: serverTimestamp(), liked: [], bookmarked: [], role: userRole.BASIC, isBlocked: false, avatar: ''
  })
}

// Returns document content
export const getUserData = async (db, col, userId) => {
  const docRef = getUserRefById(db, col, userId);
  const docSnap = await getDoc(docRef);
  return docSnap || null
}

// Return 10most active users
export const getActiveUsers = async (db, col) => {
  const q = query(collection(db, col), orderBy("comments", "desc"), limit(10));
  const querySnapshot = await getDocs(q);
  return querySnapshot
}

export const getAllData = async (app, db, col) => {
  // const ref= getInstance()
  // const querySnapshot = ref.collectionGroup('posts').get()
  // return querySnapshot
}

export const getAllUsers = async (db) => {
  const q = query(collection(db, '/Users'));
  const docSnap = await getDocs(q);
  const array = [];
  docSnap.forEach(doc=>{array.push(doc.data())})
  return array
};
