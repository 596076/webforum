import './App.css';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Navbar from "./components/Navigation/Navbar";
import Tags from './components/Posts-filter/Tags';
import Top from './components/Posts-filter/Top';
import { useState } from 'react';
import AppState from './Providers/app-state';
import Authenticated from './hoc/Authenticated';
import { Home } from './views/Home/Home.jsx';
import Register from './views/Register/Register.jsx';
import { ToastContainer } from 'react-toastify';
import NewTopic from './components/New-Topic/NewTopic';
import Login from './components/User-1/Login';
import Search from './components/Posts-filter/Search';
import {Debug} from './views/Debug/Debug.js';
import {ErrorBoundary} from './utils/Errors.js';
import {DebugProfile} from './views/Debug/DebugProfile.js';
import Profile from './views/Profile/Profile';
import {Topics} from './views/Topics/Topics.jsx';
import {DebugPosts} from './views/Debug/DebugPosts.js';
import Users from './views/Users/users.jsx'
import {Error404} from './404.jsx';


function App() {
  const [appState, setState] = useState({
    user: null,
    userData: null,
    loggedIn: !sessionStorage.getItem('isAnonymous'),
    rerender: true
  });


  return (
    <Router>
    <ErrorBoundary>
      <AppState.Provider value={{ appState, setState }}>
        <ToastContainer
          theme="colored"
          position="top-left"
          autoClose={5000}
          hideProgressBar
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover={false}
        />
        <div className='NAV'>
          <Navbar />
        </div>
        <div className='MAIN'>
          <div className='second-nav'>
            <Search/>
            <NewTopic />
          </div>
          <Routes>
            <Route index element={<Home />} />
            <Route path='/register' element={<Register />} />
            <Route path='/tags' element={<Authenticated><Tags /></Authenticated>} />
            <Route path='/home' element={<Home />} />
            {/*<Route path='/latest.short' element={<LatestShort />} />*/}
            <Route path='/users' element={<Authenticated><Users /></Authenticated>} />
            <Route path='/home/:category/topics'  element={<Topics />} />
            <Route path='/top' element={<Authenticated><Top /></Authenticated>} />
            <Route path='/login' element={<Login />} />
            <Route path='/start/debug' element={<Authenticated><Debug /></Authenticated>} />
            <Route path='/start/debug/:category/:topicuid/posts' element={<DebugPosts />} />
            <Route path='/start/debug/profile/:profile' element={<Authenticated><DebugProfile /></Authenticated>} />
            <Route path='/profile' element={<Authenticated><Profile /></Authenticated>} />
            <Route path='*' element={<Error404 />} />
            {/* <Route path='/user' element={<User setUser={setUser}/>} /> */}
          </Routes>
        </div>
      </AppState.Provider>
    </ErrorBoundary>
    </Router>
  );
}

export default App;
