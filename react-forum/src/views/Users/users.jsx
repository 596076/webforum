/* eslint-disable max-len */
/* eslint-disable no-unused-vars */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable require-jsdoc */
import './Users.css';
import {useContext, useEffect, useState} from 'react';
import {Avatar} from '@mui/material';
import { db } from '../../config/firebase-config';
import { getAllUsers, getUserById, updateUserDocumentField } from '../../services/users.service';
import TableContainer from '@mui/material/TableContainer';
import TableRow from '@mui/material/TableRow';
import TableHead from '@mui/material/TableHead';
import TableCell from '@mui/material/TableCell';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import { Button } from '@mui/material';
import Paper from '@mui/material/Paper';

const Users = () => {
  const [users, setUsers] = useState([]);
  const [searchField, setSearchField] = useState('');
  const [currentPage, setCurrentPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);
useEffect(()=>
{  (async () => {
    setUsers((await getAllUsers(db)).sort((a,b)=>a?.userName.localeCompare(b?.userName)));
  })();
},[]
)

  const handleBlock = async (userName) => {
    const currUser = await getUserById(userName);
    console.log(currUser.data().isBlocked)
    await updateUserDocumentField(db,'/Users', userName, 'isBlocked', currUser.data().isBlocked?false:true)
    setUsers(await getAllUsers(db))
  };
const columns = [
  { 
    id: 'avatar',
    label: 'avatar',
    width: '15%',
    align: 'left', 
  },
    { 
      id: 'userName',
      label: 'userName',
      width: '15%',
      align: 'left', 
    },
    {
      id: 'email',
      label: 'Email',
      width: '15%',
      align: 'right',
    },
    {
      id: 'name',
      label: 'Name',
      width: '15%',
      align: 'right',
    },
    {
      id: 'role',
      label: 'Role',
      width: '15%',
      align: 'right',
    },
    {
      id: 'block',
      label: 'Change Status',
      width: '20%',
      align: 'right',
    },
  ];

  const rows = users
  return (
    <>
    <div className="search-user-section">
                <input
                  className="search-user-bar"
                  type="search"
                  name="search-form"
                  placeholder="Search user here..."
                  id="search-addon"
                  //onChange={handleSearchValue}
                  autoComplete="off"
                />
              </div>
    <TableContainer component={Paper} sx={{display: 'flex', margin: 'auto', width: '100%', mt: 2}}>
      <Table sx={{
        width: "100%",
        display: 'flex',
        height: '65vh',
        flexDirection: 'column',
        alignItems: 'center',
        margin: 'auto', 
        }} aria-label="custom pagination table">

      <TableHead  sx={{height: '10%', width: '100%', bgcolor: '#0078aa'}}>
            <TableRow sx={{
              fontWeight: 'bold',
              display: 'flex',
              margin: 'auto',
              width: '100%',
              justifyContent: 'center',
              height: '100%',
         }}>
              {columns.map((column) => (
                <TableCell
              
                  style={{ width: column.width,  textAlign: column.align}}
                    sx={{
                      color: '#ffffff',
                      display: 'flex',
                      fontSize: '0.85em',
                      alignItems: 'center',
                      height: '58%'  
                    }}
                  key={column.id}
                  align={column.align}
                >
                  <div className='header-search-user'>{column.label}</div>
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
        <TableBody sx={{width: '100%', height: '80%', pl: 5}} >
          {(rowsPerPage > 0
            ? rows
            : rows
          ).map((row) => (
            <TableRow sx={{
              fontWeight: 'bold',
              height: '18%', 
              display: 'flex',
              margin: 'auto',
              alignItems: 'center'
            }}
            key={row.userName}
            >
              <TableCell sx={{width: '10%', display: 'flex'}}>
                <div className="table-userName">
                  {row?.avatar ?
                  <img src={`${row?.avatar}`} className='img-account' width= '40em' height= '40em' alt='avatar'
                   /> :
                  <Avatar
                    className="avatar-icon"
                    sx={{
                      bgcolor: "#a3b9d6",
                      fontSize: 20,
                      width: 40,
                      height: 40,
                      mt: 0,
                    }}
                  >
                    {row.userName.slice(0, 1).toUpperCase()}
                  </Avatar>}
                  
                </div>
              </TableCell>
              <TableCell sx={{width: '18%', display: 'flex', pl:5}}>
              <div className='userName'>{row.userName}</div>
              </TableCell>
              <TableCell sx={{width: '20%', display: 'flex'}} align="right">
                <div className="table-email">
                  {row.email}
                </div>
              </TableCell>
              <TableCell sx={{width: '20%', display: 'flex'}} align="right">
                <div className="table-names">
                  {row.firstName === '' && row.lastName === '' ? 
                  'Empty name' :
                  `${row.firstName} ${row.lastName}`
        
                  }
               </div> 
              </TableCell>
              <TableCell sx={{width: '15%', display: 'flex'}} align="right">
                  {row.role === 1 ?
                    <div className='role-basic'>Basic</div> :
                    row.role === 2 ?
                    <div className='role-admin'>Admin</div> :
                    null
                  }
              </TableCell>
              <TableCell sx={{width: '12%', display: 'flex', justifyContent: 'center'}} align="center">
                <div className="table-blocked">

                {row.isBlocked? 
                <Button onClick={() => handleBlock(row.uid)}> <div className='block-text'>Unblock</div> </Button> 
                : <Button onClick={() => handleBlock(row.uid)}> <div className='block-text'>Block</div> </Button>}
                </div>
              </TableCell>
            </TableRow>
          ))}

       
        </TableBody>
      
      </Table>
    </TableContainer>
    </>
  );
//   const usersView = (array) =>
//     array.slice(offset, offset + PER_PAGE).map((eachUser, index) => (
//       <div className="each-user" key={eachUser?.uid}>
//         <div className="avatar">
//           {eachUser?.avatar? (
//             <>
//               <div className="photoSearchUser">
//                 <img
//                   src={eachUser?.avatar}
//                   alt="profile"
//                   className="imgProfile"
//                 />
//               </div>
//               <div className="each-user-userName">{eachUser?.userName}</div>
//             </>
//           ) : (
//             <>
//               <div className="photoSearchUser">
//               <Avatar >{eachUser?.userName[0]}</Avatar>

//               </div>
//               <div className="each-user-userName">{eachUser.userName}</div>
//             </>
//           )}
//         </div>
//         <div className="user-email">{eachUser.email}</div>
//         <div className="each-user-role">
//           {eachUser.role === 1 ?
//             'Basic' :
//             eachUser.role === 2 ?
//             'Admin' :
//             eachUser.isBlocked?'Blocked':'' }
//         </div>
//         <div className="user-text-names">
//           {eachUser.firstName} {eachUser.lastName}
//         </div>

//         <button onClick={() => handleBlock(eachUser.uid)}>
//           {eachUser.isBlocked ? (
//             <RiUserAddLine className="unblocked" />
//           ) : (
//             <ImBlocked />
//           )}
//         </button>
//       </div>
//     ));

//   return (
//     <div className="Search-user">
//       <div className="search-user-header">
        
//         <div className="search-user-section">
//           <input
//             className="search-user-bar"
//             type="search"
//             name="search-form"
//             placeholder="Search user"
//             id="search-user"
//           />
//           <VscSearch  className="search-button" />
//         </div>
//       </div>

//       <div className="body-all-users">
//         <div className="user-head-menu">
//           <div>Image</div>
//           <div>user</div>
//           <div>email</div>
//           <div>role</div>
//           <div>name</div>
//           <div>status</div>
//         </div>
//         <div>
//           <hr />
//         </div>
//         {users.length !== 0 ? usersView(users):'No users to show!'
//         }
//       </div>
//     </div>
//   );
 }
export default Users; 
