import React from 'react'
import {Link} from 'react-router-dom';

class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props);
    this.state = { error: null, errorInfo: null };
  }

  componentDidCatch(error, errorInfo) {
    this.setState({
      error: error,
      errorInfo: errorInfo
    })
  }

  render() {
    if (this.state.errorInfo) {
      return (
        <div style={{color:"red",backgroundColor:"black",textAlign:"center"}}>

          <h2>Something went wrong but we know already and soon it will be fixed.</h2>
          <div ><Link to="/home" onClick={()=>window.local.reload()}>Back to Home</Link></div>
        </div>
      );
    }
    return this.props.children;
  }
}
export {ErrorBoundary}