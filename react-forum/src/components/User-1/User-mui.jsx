import './User-mui.css';
import React, {useContext, useEffect, useState} from 'react';
import Box from '@mui/material/Box';
import Avatar from '@mui/material/Avatar';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import Divider from '@mui/material/Divider';
import IconButton from '@mui/material/IconButton';
// import Typography from '@mui/material/Typography';
import Tooltip from '@mui/material/Tooltip';
import PersonAdd from '@mui/icons-material/PersonAdd';
import Settings from '@mui/icons-material/Settings';
import Logout from '@mui/icons-material/Logout';
import { Link, useNavigate } from 'react-router-dom';
import AppState from '../../Providers/app-state';
import { logoutUser } from '../../services/auth.service';
import ProfilePicture from './Profile-picture';

//TODO - use AppState to protect the page!
//TODO row 55 - user's avatars.

const AccountMenu = ({logoutFn}) => {

  // My start
  const navigate = useNavigate();
  const { appState,setState } = useContext(AppState);


  const logout = () => {
    // setState({
    //   user: null,
    //   userData: null,
    // });
    sessionStorage.clear()
    logoutFn(true)
    setState({...appState,loggedIn: false})
    sessionStorage.clear()
    logoutUser().then(()=> navigate('/home'))

  };
  // My end

  const [anchorEl, setAnchorEl] = useState(null);
  const open = Boolean(anchorEl);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  return (
    <React.Fragment>
      <Box sx={{ display: 'flex', alignItems: 'center', textAlign: 'center' }}>
        {/* <Typography sx={{ minWidth: 100 }}>Contact</Typography>
        <Typography sx={{ minWidth: 100 }}>Profile</Typography> */}
        <Tooltip
          title="Account settings"
          arrow
          componentsProps={{
            tooltip: {
              sx: {
                bgcolor: '#0078aa',
                fontFamily: 'Poppins',
                fontSize: 17,
                '& .MuiTooltip-arrow': {
                  color: '#0078aa',
                },
              },
            },
          }}
        >
          <IconButton
            onClick={handleClick}
            size="small"
            sx={{ ml: 0 }}
            aria-controls={open ? 'account-menu' : undefined}
            aria-haspopup="true"
            aria-expanded={open ? 'true' : undefined}
          >
            {/* <Avatar src={'../../assets/avatar-02.png'} className='Avatar-Nav' sx={{ width: 60, height: 60 }}></Avatar> */}
            {/* <Avatar alt='bb'src={`${process.env.PUBLIC_URL}../../assets/avatar-02.png`} className='Avatar-Nav' sx={{ width: 60, height: 60 }} /> */}
            {/* <Avatar alt='' src='' sx={{ width: 60, height: 60 }} /> */}
            {/* <div>{ProfilePicture('Boyko Denev', 60)}</div> */}
            <div>{ProfilePicture(60)}</div>

          </IconButton>
        </Tooltip>
      </Box>
      
      <Menu
        anchorEl={anchorEl}
        id="account-menu"
        open={open}
        onClose={handleClose}
        onClick={handleClose}
        PaperProps={{
          elevation: 0,
          sx: {
            background: "#f6f6f6",
            overflow: 'visible',
            filter: 'drop-shadow(0px 2px 8px rgba(0,0,0,0.32))',
            mt: 1.5,
            ml: -2,
            '& .MuiAvatar-root': {
              background: "#0078aa",
              width: 22,
              height: 22,
              ml: 0,
              mr: 1.5,
            },
            '&:before': {
              content: '""',
              display: 'block',
              position: 'absolute',
              top: 0,
              right: 14,
              width: 10,
              height: 10,
              bgcolor: 'background.paper',
              transform: 'translateY(-50%) rotate(45deg)',
              zIndex: 0,
            },
          },
        }}
        transformOrigin={{ horizontal: 'right', vertical: 'top' }}
        anchorOrigin={{ horizontal: 'right', vertical: 'bottom' }}
      >

        <Link to="/profile" style={{ textDecoration: 'none' }}>
          <MenuItem sx={{textDecoration: 'none', fontFamily: 'Poppins', fontSize: 17, color: "#0078aa"}}>
            <Avatar /> Profile
          </MenuItem>
        </Link>
        <MenuItem sx={{fontFamily: 'Poppins', fontSize: 17, color: "#0078aa"}}>
          <Avatar /> My account
        </MenuItem>
        <Divider />
        <MenuItem sx={{fontFamily: 'Poppins', fontSize: 17, color: "#0078aa"}}>
          <ListItemIcon>
            <PersonAdd sx={{ fontSize: 24, color: "#0078aa"}}/>
          </ListItemIcon>
          Add another account
        </MenuItem>
        <MenuItem sx={{fontFamily: 'Poppins', fontSize: 17, color: "#0078aa"}}>
          <ListItemIcon>
            <Settings sx={{ fontSize: 24, color: "#0078aa" }} />
          </ListItemIcon>
          Settings
        </MenuItem>
        <MenuItem onClick={logout} sx={{fontFamily: 'Poppins', fontSize: 17, color: "#0078aa"}}>
          <ListItemIcon>
            <Logout sx={{ fontSize: 24, color: "#0078aa" }} />
          </ListItemIcon>
          Logout
        </MenuItem>
      </Menu>
    </React.Fragment>
  );
}

export default AccountMenu;
