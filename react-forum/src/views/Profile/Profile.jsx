import './Profile.scss';
import React, { useState, useEffect } from 'react';
import { auth, db } from '../../config/firebase-config';
import { getUserData, updateUserById } from '../../services/users.service';
import ProfilePicture from '../../components/User-1/Profile-picture';
import { IconName } from "react-icons/fa";
// import Avatar from '@mui/material/Avatar';

const Profile = () => {
  const userSession = sessionStorage.getItem('username');
  const userId = auth.currentUser?.uid;
  const [loading, setLoading] = useState(true);
  const [userData, setUserData] = useState({});
  const [editFirstName, setEditFirstName] = useState(false);
  const [editLastName, setEditLastName] = useState(false);
  const [user, setUser] = useState('');

  useEffect(()=>{
      setUser(userSession)
  },[userSession])

  useEffect(() => {
    getUserData(db, '/Users', userId)
      .then((resp) => {
        setUserData(resp.data());
        setLoading(false)
      })
      .catch((error) => 
      console.log(error.message));
  }, [loading, userId]);

  return (
    <div className='profile-box'>
      <h1 className='profile-title'>Profile</h1>

      {/* <Avatar alt={userData.userName.toUpperCase()} src='userData.avatar' className='Avatar-Nav' sx={{ width: 60, height: 60 }} /> */}
      <div className='profile-items'>

          <div className='subtitle'>Username</div>
          <div className='subcontent'>
            {userData.userName}
          </div>

          <hr className="line"></hr>

          <div className='subtitle'>Profile Picture</div>
            <div className='subcontent'>
              <div className='prof-picture'>{ProfilePicture(80)}</div>
              <div className='edit' onClick={() => setEditLastName(true)}><i class="fa-solid fa-pencil"></i></div>
            </div>
            
          <hr className="line"></hr>

          <div className='subtitle'>Email</div>
          <div className='subcontent'>
            {userData.email}
          </div>

          <hr className="line"></hr>

          <div className='subtitle'>First Name</div>
          <div className='subcontent'>
            <div className='info'>
              {editFirstName
                ? <input defaultValue={userData.firstName} onChange={(e) => setUserData({...userData, firstName: e.target.value})} onBlur={() => setEditFirstName(false)}></input>
                : <div>{userData.firstName}</div>
              }
            </div>
            <div className='edit' onClick={() => setEditFirstName(true)}><i class="fa-solid fa-pencil"></i></div>
          </div>

          <div className='subtitle'>Last Name</div>
          <div className='subcontent'>
            <div className='info'>
              {editLastName
                ? <input defaultValue={userData.lastName} onChange={(e) => setUserData({...userData, lastName: e.target.value})} onBlur={() => setEditLastName(false)}></input>
                : <div>{userData.lastName}</div>
              }
            </div>
            <div className='edit' onClick={() => setEditLastName(true)}><i class="fa-solid fa-pencil"></i></div>
          </div>

      </div>
      

      {/* <button onClick={login}>Log In</button> */}
      <button onClick={() => updateUserById(db, '/Users', userId, userData).catch((e) => console.log(e.message))}>Save Changes</button>
      
      {/* <p>{getAllInCollection}</p> */}
    </div>
  )
}

export default Profile;