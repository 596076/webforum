import React, {useEffect, useState} from 'react';
import './Categories-topic.css';
import AsyncSelect from 'react-select/async';
import Select from 'react-select';
import {getAllCategories} from '../../services/topics.service.js';
import {createPostToTopicRecord} from '../../services/posts.service.js';
import {auth, db} from '../../config/firebase-config.js';
import {addDoc, collection, serverTimestamp} from 'firebase/firestore';
// import { defaultStyles } from 'react-select/dist/declarations/src/styles';

//TODO - use AppState to protect the page!

  // const categories1 = [
  //   {label: 'Category 1', value: 1},
  //   {label: 'Category 2', value: 2},
  //   {label: 'Category 3', value: 3},
  //   {label: 'Category 4', value: 4},
  //   {label: 'Category 5', value: 5},
  //   {label: 'Category 6', value: 6},
  //   {label: 'Category 7', value: 7},
  //   {label: 'Category 8', value: 8},
  //   {label: 'Category 9', value: 9},
  // ];

  const customStyles = {
    option: (provided, state) => ({
      ...provided,
      borderBottom: '1px solid #d4d4d4',
      color: state.isSelected ? 'white' : '#0078aa',
      padding: 10,
    }),

    control: (style, state) => ({
      ...style,
      borderRadius: 22,
      borderColor: '#0078aa',
      borderWidth: '2px',
    }),

    singleValue: (base, state) => ({
      ...base,
      color: '#0078aa',
      fontFamily: 'Poppins',
      fontWeight: '500',
    }),

    input: style => ({
      ...style,
      color: '#0078aa',
    }),

    placeholder: styles => ({
        ...styles,
        color: '#ababab',
    })
  };

  const CategoriesNewTopic = ({setCategory}) => {
    // const [categories, setCategories] = useState([])
    const [loadOptions, setLoadOptions] = useState([]);
    // const [tagValue, setTagValue] = useState('');

    useEffect(()=>{
      getAllCategories().then(cats=>{
        // const tempArr = []
        const tempArr2 = []
        // cats.forEach(c=>tempArr.push(c.id))
        // setCategories(tempArr)
        cats.forEach(c => tempArr2.push({value: c.id, label: c.id}))
        setLoadOptions(tempArr2)
      })
    },[])

    const handleChange = (event, field) => {

          setCategory(event.value);


    }

    // const handleChange = event => {
    //   setCategory(event.value);
    // };

    return (
      <div className='category-container-topic'>
        <div className='category-form-topic'>
          <div className='category-input-topic'>
            {/* <select defaultValue='Android' onChange={(e)=>setCategory(e.target.value)}>
              {categories.map(cat=>(
                <option value={cat} key={cat}>{cat}</option>
              ))}
            </select> */}
            <Select
              placeholder='Categories...'
              isClearable
              onChange={handleChange}
              options={loadOptions}
              styles={customStyles}
            />
          </div>
        </div>
      </div>
    )
  }

export default CategoriesNewTopic;
