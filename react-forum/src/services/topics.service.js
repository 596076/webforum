import {
  collection,
  collectionGroup,
  doc,
  getDocs,
  getDoc,
  increment,
  orderBy,
  query,
  updateDoc,
  where, serverTimestamp,limit
} from 'firebase/firestore';
import {db} from '../config/firebase-config.js';

export const getActiveTopics = () => {

  // const data = query(collectionGroup(db, 'topics'), orderBy("lastEdit", "desc"),limit(10));
  const data = query(collectionGroup(db, 'topics'), orderBy("lastEdit", "desc"));
  return getDocs(data).then(querySnapshot=>{
      const mapData = [];
      querySnapshot.forEach((doc) => {
      //   mapData.push({id: doc.id, category: doc.ref.parent.parent.id, data: doc.data()});
          mapData.push(doc)
      });
      return mapData;
    }
  );
};

export const returnAllCategoryTopics = () => {

  const data = query(collectionGroup(db, 'topics'), orderBy("createDate", "desc"));
  return getDocs(data).then(querySnapshot => {
      const mapData = [];
      querySnapshot.forEach((doc) => {
        mapData.push({id: doc.id, category: doc.ref.parent.parent.id, data: doc.data()});
      });
      return mapData;
    }
  );
};

export const returnPostsByMultipleTags = (arr) => {

  const data = query(collectionGroup(db, 'topics'), where('tags', "array-contains-any", arr), orderBy("createDate"));
  getDocs(data).then(querySnapshot => {
      const mapData = [];
      querySnapshot.forEach((doc) => {
        mapData.push({id: doc.id, data: doc.data()});
      });
      return mapData;
    }
  );
};


export const updateTopicCounter = (category,topicuid) =>{
  // console.log(JSON.stringify(topicuid),JSON.stringify(category))
  return updateDoc(doc(db,`/Categories/${category}/topics/`,topicuid),{numOfPosts:increment(1),lastEdit:serverTimestamp()})
    .catch(err=>console.log(err.message()))
}

export const getTopicData = (category,topicuid) => {
  return getDoc(doc(db,`/Categories/${category}/topics/`,topicuid))
}

export const getAllCategories=()=>{
  return getDocs(collection(db,'/Categories'))
}

export const getAllSearchTopics=(search)=>{
  return getDocs(query(collection(db, '/Categories/Android/topics'), where('title', "==", search)))
}