import React, { useState } from 'react';
import './Tags-topic.css';
import Creatable from 'react-select/creatable';
// import Select from 'react-select';

// import { defaultStyles } from 'react-select/dist/declarations/src/styles';


//TODO - use AppState to protect the page!

  const tags = [
    {label: 'Tag 1', value: 1},
    {label: 'Tag 2', value: 2},
    {label: 'Tag 3', value: 3},
    {label: 'Tag 4', value: 4},
    {label: 'Tag 5', value: 5},
    {label: 'Tag 6', value: 6},
    {label: 'Tag 7', value: 7},
    {label: 'Tag 8', value: 8},
    {label: 'Tag 9', value: 9},
  ];

  const customStyles = {
    option: (provided, state) => ({
      ...provided,
      borderBottom: '1px solid #d4d4d4',
      // color: state.isSelected ? 'red' : '#0078aa',
      color: '#0078aa',
      padding: 10,
    }),

    control: (style, state) => ({
      ...style,
      borderRadius: 22,
      borderColor: '#0078aa',
      borderWidth: '2px',
      color: '#d4d4d4',
      justify: 'top',
      with: '180px',
    }),
      
    // menu: styles => ({
    //   ...styles,
    //   width: '400px',
    // }),

    // input: (style, state) => ({
    //   ...style,
    //   color: '#0078aa',
    // }),

    // multiValue: (style) => ({
    //   ...style,
    //   // background: 'red',
    //   display: 'flex',
    //   // margin: '0px',
    // }),

    multiValueLabel: (style) => ({
      ...style,
      color: '#0078aa',
      outerHeight: 5,
      height: '20px',
      padding: '0 10px',
      
    }),

    multiValueRemove: (style) => ({
      ...style,
      background: '#d4d4d4',
      color: '#0078aa',
      height: '20px',
      ali: 'center',
    }),

    placeholder: styles => ({
        ...styles,
        color: '#ababab',
    })
  };

  const TagsNewTopic = (props) => {
    const [tagValue, setTagValue] = useState('');
    // const [tagInputValue, setTagInputValue] = useState('');
    // const [tagValue, setTagValue] = useState('');
  
    const handleChange = (field, value) => {
      switch (field) {
        case 'tags':
          setTagValue(value);
          break;
      
        default:
          break;
      }
    }

    // const handleKeyDown = event => {
    //   if (!tagInputValue) return;

    //   switch (event.key) {
    //     case 'Enter':
    //     case 'Tab':
    //       setTagValue([...tagValue, createOption(tagInputValue)]);
    //       setTagInputValue('');

    //       event.preventDefault();
    //       break;
      
    //     default:
    //       break;
    //   }
    // }

    // const createOption = label => ({
    //   label,
    //   value: label
    // })

    // const handleInputChange = (value) => {
    //   setTagInputValue(value);
    // }

    return (
      <div className='tag-container-topic'>
        <div className='tag-form-topic'>
          <div className='input-topic'>
            <Creatable
              // color='blue'
              placeholder='Tag(s)...'
              isMulti
              onChange={(value) => handleChange('tags', value)}
              options={tags}
              value={tagValue}
              styles={customStyles}
            />
            {/* <Creatable
              placeholder='Tag'
              isClearable
              isMulti
              components={
                {DropdownIndicator: null}
              }
              inputValue={tagInputValue}
              menuIsOpen={false}
              onChange={(value) => handleChange('tags', value)}
              onKeyDown={handleKeyDown}
              onInputChange={handleInputChange}
              value={tagValue}
              styles={customStyles}
            /> */}
          </div>
        </div>
      </div>
    )
  }

export default TagsNewTopic;
