import './Register.scss';
import { Link, useLocation, useNavigate } from 'react-router-dom';
import { useState, useEffect } from 'react';
import {toast} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import {createUser} from '../../services/users.service.js';
import { createUserWithEmailAndPassword } from 'firebase/auth';
import { auth } from '../../config/firebase-config';

// TODO Sign pu Form! (Switch between Login and Sign up)

const Register = () => {
  // const [error, setError] = useState(false);
  // const [email, setemail] = useState('');
  // const [password, setPassword] = useState('false');

  const handleSignup = (e) => {
    e.preventDefault();

    // createUserWithEmailAndPassword(auth, email, password)
    // .then((userCredential) => {
    //   // Signed in
    //   return userCredential
    // })
    // .catch ((error) => {
    //   const errorCode = error.code;
    //   const errorMessage = error.message;
    // })

  }

  const [data, setData] = useState({});
  const [success, setSuccess] = useState(null);
  const [message, setMessage] = useState(null);

  const navigate = useNavigate();
  const location = useLocation();

  const showMessage = (suc, msg) => {
    if (suc === true) {
      return toast.success(msg, {
        position: "top-left",
        autoClose: 5000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: false,
        draggable: true,
        progress: undefined,
      });
    } else if (suc === false) {
      return toast.error(msg, {
        position: "top-left",
        autoClose: 5000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: false,
        draggable: true,
        progress: undefined,
      });
    }
    // setSuccess(null)
  };

  return (
    <>
      {useEffect(() => {
        showMessage(success, message);
        return () => setSuccess(null);
      }, [success, message])    // ??? message

      }

      <div className='signup'>
        <form onSubmit={handleSignup}>
          <h2>Sign Up</h2>
          <hr className="new1"></hr>
          <input type='text' placeholder='username' onChange={(e) => setData({...data, handle: e.target.value})}/>
          <input type='email' placeholder='email' onChange={(e) => setData({...data, email: e.target.value})}/>
          <input type='password' placeholder='password' onChange={(e) => setData({...data, password: e.target.value})}/>
          {/* {error && <span>Wrong email or password!</span>} */}
          <button
            onClick={() => {
              createUser(data).then(resp => {
                setSuccess(true);
                setMessage("User created successfully");
                console.log(resp?.user?.uid);
                // navigate login
                navigate(location?.state?.from ?? '/login', {replace: true});
              }).catch((err) => {
                setMessage(err.code);
                setSuccess(false);
                // setTimeout(()=> setSuccess(null),5000)
              });
            }} 
            // endIcon={<SendIcon/>}
          >
          Sign Up</button>
          <hr className="new2"></hr>
            <div className='not-a-member'>
              <p style={{marginRight: "15px"}}>You have an account?</p>
              <p><Link to="/login" className='login-signup'>Log In</Link></p>
            </div>
        </form>
      </div>
    </>
  )
};

export default Register;
// export default Register;



// ---------------------------------



// import "./Register.css";
// import {useEffect, useState} from 'react';
// import Box from '@mui/material/Box';
// import TextField from '@mui/material/TextField';
// import SendIcon from '@mui/icons-material/Send';
// import Button from '@mui/material/Button';
// import {toast} from 'react-toastify';
// import 'react-toastify/dist/ReactToastify.css';
// import {createUser} from '../../services/users.service.js';

// export const Register = () => {
//   const [data, setData] = useState({});
//   const [success, setSuccess] = useState(null);
//   const [message, setMessage] = useState(null);
//   const showMessage = (suc, msg) => {
//     if (suc === true) {
//       return toast.success(msg, {
//         position: "top-left",
//         autoClose: 5000,
//         hideProgressBar: true,
//         closeOnClick: true,
//         pauseOnHover: false,
//         draggable: true,
//         progress: undefined,
//       });
//     } else if (suc === false) {
//       return toast.error(msg, {
//         position: "top-left",
//         autoClose: 5000,
//         hideProgressBar: true,
//         closeOnClick: true,
//         pauseOnHover: false,
//         draggable: true,
//         progress: undefined,
//       });
//     }
//     // setSuccess(null)
//   };
//   return (
//     <>
//       {useEffect(() => {
//         showMessage(success, message);
//         return () => setSuccess(null);
//       }, [success])

//       }
//       <div className="register-container">
//         <div className="register-header">

//         </div>
//         <div className="register-forms">
//           <fieldset>
//             <legend className="register-label">Register account</legend>
//             <Box
//               component="form"
//               sx={{
//                 '& .MuiTextField-root': {m: 1, width: '55ch'},
//               }}

//               noValidate
//               autoComplete="off"
//             >
//               <div className="reg-input-component">
//                 <label htmlFor="register-email"><span>email:</span></label>
//                 <TextField
//                   required
//                   id="register-email"
//                   label="email"
//                   onChange={(e) => setData({...data, email: e.target.value})}
//                 />
//               </div>
//               <div className="reg-input-component">
//                 <label htmlFor="register-password">Password:</label> <TextField required id="register-password"
//                                                                                 label="Password" variant="outlined"
//                                                                                 type="password"
//                                                                                 onChange={(e) => setData({
//                                                                                   ...data,
//                                                                                   password: e.target.value
//                                                                                 })}/></div>
//               <div className="reg-input-component">
//                 <label htmlFor="register-fname">
//                   <span>First name:</span>
//                 </label>
//                 <TextField
//                   required
//                   id="register-fname"
//                   label="First Name"
//                   onChange={(e) => setData({...data, fname: e.target.value})}
//                 />

//               </div>
//             </Box>

//           </fieldset>
//         </div>
//         <Button variant="contained" onClick={() => {
//           createUser(data).then(resp => {
//             setSuccess(true);
//             setMessage("User created successfully");
//             console.log(resp?.user?.uid);
//           }).catch((err) => {
//             setMessage(err.code);
//             setSuccess(false);
//             // setTimeout(()=> setSuccess(null),5000)
//           });
//         }} endIcon={<SendIcon/>}>
//           Submit
//         </Button>
//       </div>
//     </>
//   );
// };