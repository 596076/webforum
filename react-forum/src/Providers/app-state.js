import { createContext } from "react";

const AppState = createContext({
  appState: {
    user: null,
    userData: null
  },

  setState: () => { },
});

export default AppState;