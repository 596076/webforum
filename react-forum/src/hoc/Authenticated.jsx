import { useContext } from "react";
import { Navigate, useLocation } from "react-router-dom";
import AppState from "../Providers/app-state";

const Authenticated = ({children}) => {
  const { appState } =useContext(AppState);
  const user = sessionStorage.getItem('username');

  const location = useLocation();
  if (!(user || sessionStorage.getItem('loggedIn'))) {
      <h3>Need to login</h3>
    {console.log('not authenticated')}
    return (
      <Navigate to='/home' state={{ from: location.pathname }}/>
    )
  }

  return children;
}

export default Authenticated;