import "./Navbar.css";
import { useState, useEffect, useContext } from "react";
import { Link, NavLink } from "react-router-dom";
import { HiOutlineMenu } from "react-icons/hi";
import { CgClose } from "react-icons/cg";
import AccountMenu from "../User-1/User-mui";
import AppState from "../../Providers/app-state";
import Tags from "../Posts-filter/Tags";
import Categories from "../Posts-filter/Categories";
import { userRole } from "../../common/user-role";
const Navbar = () => {
  const [open, setOpen] = useState(false);
  const [width, setWidth] = useState(0);
  const { appState } =useContext(AppState);
  const userData = appState.userData;
  const userSession = sessionStorage.getItem('username');
  const [user, setUser] = useState('');

  useEffect(()=>{
      setUser(userSession)
  },[userSession])


  useEffect(() => {
    const handleWidth = () => {
      setWidth(window.innerWidth);
    };

    window.addEventListener("resize", handleWidth);
 
    return () => window.removeEventListener("resize", handleWidth);
  }, []);
  useEffect(() => {
    if (width > 769) {
      setOpen(false);
    }
  }, [width, open]);

  return (
    <header>
      <div className="nav-container">
        <div className="navbar">
          <Link to="/" className="nav-logo">Forum</Link>
          <div className="nav-menu-and-btn">
            <ul className={`nav-menu ${open ? "show" : ""}`}>
              <div className="categories-box">
                {user
                  ? <Categories />
                  : null
                }
              </div>
              <div className="tags-box">
                {user
                  ? <Tags />
                  : null
                }
              </div>
              <div className="nav-menu--all-items">
                <li onClick={() => setOpen(false)}>
                  <NavLink to="/home" className="nav-menu--item">Latest</NavLink>
                </li>
                <li onClick={() => setOpen(false)}>
                  <NavLink to="/top" className="nav-menu--item">Top</NavLink>
                </li>
                {(user&&userData?.role===userRole.ADMIN)
                  ?  (<li onClick={() => setOpen(false)}>
                  <NavLink to="/users" className="nav-menu--item">Users</NavLink>
                </li>)
                  : null
                }
               
              </div>
            </ul>
            {/* {user
              ? <div className="search-box">
                  <input type="search" placeholder="Search..."/>  
                </div>
              : null
            } */}
            <button className="toggle-btn" onClick={() => setOpen((prev) => !prev)}>
              {open ? <CgClose /> : <HiOutlineMenu />}
            </button>
            <div className="user-avatar">
              {!user
                ? <Link to="/login">
                  <div className="user-login"></div>
                  </Link>
                : <AccountMenu logoutFn={setUser} />}
            </div>
          </div>
        </div>
      </div>
    </header>
  );
};

export default Navbar;
