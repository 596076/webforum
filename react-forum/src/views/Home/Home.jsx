import './Home.css';
import {useContext, useEffect, useState} from 'react';
import {
  getActiveTopics,
  getAllCategories,
  getAllSearchTopics,
  returnAllCategoryTopics
} from '../../services/topics.service.js';
import { Link } from "react-router-dom";
import {AiOutlineApple,AiOutlineAndroid} from 'react-icons/ai'
import {IoIosHelpCircleOutline} from 'react-icons/io'
import {BsController} from 'react-icons/bs'
import {Latest} from '../../components/Posts-filter/Latest.jsx';
import AppState from '../../Providers/app-state.js';
export const Home = () => {
  const [categories, setCategories] = useState([]);
  const [topics,setTopics] = useState([])
  const {appState} = useContext(AppState)



  useEffect(()=>{
    getActiveTopics().then(topics=>setTopics(topics))
  },[appState])


  useEffect(() => {
    getAllCategories().then(cats => {
      const data = [];
      cats.forEach(cat => data.push(cat));
      setCategories(data);
    });
  }, []);
  const returnIcon=(name)=>{
    switch (name){
      case 'iOS':
        return <AiOutlineApple/>

      case 'Android':
        return <AiOutlineAndroid/>
      case 'Games':
        return <BsController />
      case 'Guide':
        return <IoIosHelpCircleOutline />

    }
  }
  return (
    <div className="Home-container">
      <div className="Category-container">
        <h2>Category:</h2>
        {console.log('v0.1')}
        {categories.map(category =>
          <div className="cat1" key={category.id}>
            <Link to={`/home/${category.id}/topics`}>{returnIcon(category.id)}</Link>
            <p style={{width:"80px",textAlign:"center"}}>{category.data().description}</p>
          </div>)}
      </div>
       <Latest topics={topics} />
    </div>
  );
};