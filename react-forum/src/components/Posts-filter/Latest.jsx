import './Latest.css'
import React, {useEffect, useState} from 'react';
import {Link} from 'react-router-dom';
import moment from 'moment';
import Pagination from '@mui/material/Pagination';
import Stack from '@mui/material/Stack';
//TODO - use AppState to protect the page. Only first 10 posts for anonymous!

export const Latest = ({topics}) => {
  const tempArr = topics.slice(0,10)
  const [latestTopics,setLatestTopics] = useState([])
  useEffect(()=>{
    setLatestTopics(topics.slice(0,10))
  },[topics])

  const renderTopics = (page) =>{
    page=page-1
    setLatestTopics(topics.slice(page*10,(page+1)*10))
  }

  const returnTopics = (parTopics) =>{
    const numTopics = sessionStorage.getItem('isAnonymous')?Infinity:parTopics
    return latestTopics.slice(0,numTopics).map(topic=>(
      <div className="topic" key={topic.id}>
        <Link to={`/start/debug/${topic.ref.parent.parent.id}/${topic.id}/posts`}><h3>{topic.data().title}</h3></Link>
        <div><span>Category:</span><span>{topic.ref.parent.parent.id}</span></div>
        <div><span>created:</span><span className='dateSince'>{moment(topic.data().createDate.toDate()).fromNow()}</span></div>
      </div>
    ))

  }
  function BasicPagination() {
    return (
      <Stack spacing={2}>
        <Pagination count={Math.ceil(topics.length/10)} onClick={(e)=>{renderTopics(e.target.textContent)}}/>
      </Stack>
    );
  }
  return (

      <div className="cat1-full-width Latest-topics">
        <h2 style={{textAlign:"center"}}>Latest:</h2>
        <hr/>
        { returnTopics(4) }
        { sessionStorage.isAnonymous && BasicPagination() }
      </div>

  )
}
